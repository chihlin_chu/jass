import { Type } from "./type";

export interface Value{
  type: Type;
  name:string;
}